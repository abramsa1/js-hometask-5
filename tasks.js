'use strict';
/*
    В этом задании нельзя менять разметку, только писать код в этом файле.
 */

/**
*   1. Удали со страницы элемент с id "deleteMe"
**/

function removeBlock() {
  document.getElementById('deleteMe').remove();
}

/**
 *  2. Сделай так, чтобы во всех элементах с классом wrapper остался только один параграф,
 *  в котором будет сумма чисел из всех параграфов.
 *
 *  Например, такой элемент:
 *
 *  <div class="wrapper"><p>5</p><p>15</p><p>25</p><p>35</p></div>
 *
 *  должен стать таким
 *
 *  <div class="wrapper"><p>80</p></div>
 */

function calcParagraphs() {
  const wrappers = document.querySelectorAll('.wrapper');
  for (const wrapper of wrappers) {
    const sum = Array.from(wrapper.querySelectorAll('p')).reduce((acc, p) => {
      acc += Number(p.textContent);
      p.remove();
      return acc;
    }, 0);
    
    const paragraph = createElementWithText('p', sum);
    wrapper.appendChild(paragraph);
  }
}

function createElementWithText(type, text) {
  const element = document.createElement(type);
  element.textContent = text;    
  return element;
}

/**
 *  3. Измени value и type у input c id "changeMe"
 *
 */

function changeInput() {
  const input = document.getElementById('changeMe');
  input.type = 'text';
  input.value =  'Hello';
}

/**
 *  4. Используя функции appendChild и insertBefore дополни список с id "changeChild"
 *  чтобы получилась последовательность <li>0</li><li>1</li><li>2</li><li>3</li>
 *
 */

function appendList() {
  const list = document.getElementById('changeChild');
  const listItems = list.querySelectorAll('li');

  list.insertBefore(createElementWithText('li', 1), listItems[1]);
  list.appendChild(createElementWithText('li', 3));
}

/**
 *  5. Поменяй цвет всем div с class "item".
 *  Если у блока class "red", то замени его на "blue" и наоборот для блоков с class "blue"
 */

function changeColors() {
  const items = document.querySelectorAll('.item');
  for (const item of items) {
    if (item.classList.contains('red')) {
      item.classList.remove('red');
      item.classList.add('blue');
    } else if (item.classList.contains('blue')) {
      item.classList.remove('blue');
      item.classList.add('red');        
    }
  }
}

removeBlock();
calcParagraphs();
changeInput();
appendList();
changeColors();
